<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="index.css">
  <title>Peligor's Resume</title>
  <style>
  div.image{
    width: 400px;
    margin: auto;
  }
  h1.name{
    width: 400px;
    margin: auto;
  }
  div.content{
    width:400px;
    margin: auto;
  }
  div.personal-data{
    width: 500px;
    margin: auto;
  }
  table.pData{
    width: 500px;
    margin: auto;
    border: 2px solid pink;
  }
  div.Educational-Attainment{
    width: 500px;
    margin: auto;
  }
  table.EAtt{
    width: 500px;
    margin: auto;
    border: 2px solid pink;
  }
  div.Skills{
    width: 500px;
    margin: auto;
  }
  table.pSkill{
    width: 500px;
    margin: auto;
    border: 2px solid pink;
    margin-bottom: 10px;
  }
  div.whole{
    width: 600px;
    margin: auto;
    border: 5px solid lightskyblue;
    
  }
  </style>
</head>
<body>
<div class = "whole">
        <h3 class = "heading  ">Resume</h3>
  <div class = "image">
    <img  src="https://scontent.fcgy1-1.fna.fbcdn.net/v/t1.0-9/22279575_386551161748194_7368282106282758916_n.jpg?_nc_cat=0&oh=5085e76dcfe471da5d615679abf7d694&oe=5BEE5A55" height="140" width="120" align="right">
  </div>
  <div id = "section-left">
      <div class  = "section intro">
        <br>  
        <h1 class ="name">Richard Peligor</h1>
        <br>
        <div class = "content">
          <span class  = "mn"><i class="fa fa-phone"></i>   09099620374</span><br>
          <span class = "status"><i class="fa fa-drivers-license"></i> 2015-21504</span><br>
          <span class = "email"><i class="fa fa-envelope"></i>   peligorrichard@gmail.com</span><br>
          <span class = "obs">To work in any position</span>
        </div>
      </div>
    </div>
  <br>
  <br>
  <div class = "personal-data" > Personal Information</div>
  <table class = "pData">
    <tr><td class = "age">Age:</td>
      <td class = "nAge">19 years old</td><tr>
      <tr><td class = "bod">Birthday:</td>
      <td class = "nBod">April 11 1999</td></tr>
      <tr><td class = "gender">Gender:</td>
      <td class = "nGender">Male</td></tr>
      <tr><td class = "add">Address</td>
      <td class = "nAdd">BLVD, Davao City</td></tr>
  </table>
  <br>
  <div class = "Educational-Attainment">Educational Attainment</div>
  <table class = "EAtt">
    <tr><td class = "Elem">Elementary:</td>
      <td class = "nElem">Jose P. Rizal Elementary School</td></tr>
      <tr><td class = "Secon">HighSchool:</td>
      <td class = "nSecon">Lorenzo Latawan National HighSchool</td></tr>
      <tr><td class = "College">College:</td>
      <td class = "nCollege">University of SouthEasthern Ph</td></tr>
  </table>
  <br>
  <div class = "Skills">Skills</div>
  <table class = "pSkill">
    <tr><td class = "first"><ul>
      <li>Computer literate</li>
      <li>Can use C++ or Java</li> 
    </ul></td></tr>
  </table>
</div>
</body>
</html>